FROM php:8.3-fpm-alpine as build

RUN apk add --no-cache freetype-dev libjpeg-turbo-dev libpng-dev icu-dev libzip-dev icu-data-full linux-headers \
    &&  docker-php-ext-configure intl \
    &&  docker-php-ext-install pdo pdo_mysql gd intl zip opcache bcmath \
    &&  apk add --no-cache $PHPIZE_DEPS \
    &&  pecl install -o -f redis \
    &&  rm -rf /tmp/pear \
    &&  echo "extension=redis.so" > /usr/local/etc/php/conf.d/redis.ini \
    &&  apk add git \
    &&  mkdir /code \
    &&  cd /code \
    &&  curl -sS https://getcomposer.org/installer | php

WORKDIR /code

